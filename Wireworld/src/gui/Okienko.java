package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;

import java.awt.Choice;

import javax.swing.JRadioButtonMenuItem;

import java.awt.List;

import javax.swing.JCheckBox;

public class Okienko extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField textField;
	private JFileChooser wyborPliku;
	private JRadioButton rdbtnNewRadioButton_4;
	private JRadioButton rdbtnNewRadioButton_5;
	private JRadioButton rdbtnNewRadioButton_6;
	private JRadioButton rdbtnNewRadioButton_7;
	private JRadioButton rdbtnNewRadioButton;
	private JRadioButton rdbtnNewRadioButton_1;
	private JRadioButton rdbtnNewRadioButton_2;
	private JRadioButton rdbtnNewRadioButton_3;
	private JMenuItem mntmOtworzPlik;
	private ButtonGroup bgBramki;
	private ButtonGroup bgElementy;
	private ButtonGroup bgCzas;
	private JMenuItem mntmZapiszPlik_1;
	private JMenuItem mntmInstrukcja;
	private JButton btnStart;
	private JButton btnStop;
	private JButton btnNastpnaGeneracja;
	private JButton btnWyczyPlansze;
	private JRadioButton rdbtnSekunda;
	private JRadioButton 
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Okienko frame = new Okienko();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Okienko() {
		
		wyborPliku = new JFileChooser();
		bgBramki = new ButtonGroup();
		bgElementy = new ButtonGroup();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 700);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnOpcje = new JMenu("Opcje");
		menuBar.add(mnOpcje);
		
		mntmOtworzPlik = new JMenuItem("Otw\u00F3rz plik");
		mnOpcje.add(mntmOtworzPlik);
		mnOpcje.addActionListener(this);
		
		mntmZapiszPlik_1  = new JMenuItem("Zapisz plik");
		mnOpcje.add(mntmZapiszPlik_1);
		mntmZapiszPlik_1.addActionListener(this);
		
		JMenu mnPomoc = new JMenu("Pomoc");
		menuBar.add(mnPomoc);
		mnPomoc.addActionListener(this);
		
		mntmInstrukcja = new JMenuItem("Instrukcja");
		mnPomoc.add(mntmInstrukcja);
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Obraz panel = new Obraz();
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 600, 600);
		contentPane.add(panel);
		
		btnStart = new JButton("Start");
		btnStart.setBounds(628, 30, 146, 35);
		contentPane.add(btnStart);
		btnStart.addActionListener(this);
		
		btnStop = new JButton("Stop");
		btnStop.setBounds(628, 76, 146, 35);
		contentPane.add(btnStop);
		btnStop.addActionListener(this);
		
		btnNastpnaGeneracja = new JButton("Nast\u0119pna generacja");
		btnNastpnaGeneracja.setBounds(628, 122, 146, 35);
		contentPane.add(btnNastpnaGeneracja);
		btnNastpnaGeneracja.addActionListener(this);
		
		JLabel lblLiczbaGeneracji = new JLabel("LIczba generacji:");
		lblLiczbaGeneracji.setBounds(638, 182, 136, 26);
		contentPane.add(lblLiczbaGeneracji);
		
		textField = new JTextField();
		textField.setBounds(636, 214, 138, 35);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblElementy = new JLabel("Elementy:");
		lblElementy.setBounds(638, 269, 82, 16);
		contentPane.add(lblElementy);
		
		btnWyczyPlansze = new JButton("Wyczy\u015B\u0107 plansze");
		btnWyczyPlansze.setBounds(628, 549, 146, 35);
		contentPane.add(btnWyczyPlansze);
		btnWyczyPlansze.addActionListener(this);
		
		JLabel lblPokaZInterwaem = new JLabel("Poka\u017C z interwa\u0142em mi\u0119dzy ruchami:");
		lblPokaZInterwaem.setBounds(10, 611, 206, 16);
		contentPane.add(lblPokaZInterwaem);
		
		// grupa element�w
		bgElementy.add(rdbtnNewRadioButton);
		bgElementy.add(rdbtnNewRadioButton_1);
		bgElementy.add(rdbtnNewRadioButton_2);
		bgElementy.add(rdbtnNewRadioButton_3);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("G\u0142owa");
		rdbtnNewRadioButton.setBounds(612, 297, 65, 18);
		contentPane.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.addActionListener(this);
				
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Ogon");
		rdbtnNewRadioButton_1.setBounds(612, 327, 65, 18);
		contentPane.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.addActionListener(this);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Przewodnik");
		rdbtnNewRadioButton_2.setBounds(689, 327, 88, 18);
		contentPane.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.addActionListener(this);
		
		JRadioButton rdbtnNewRadioButton_3 = new JRadioButton("Pusta");
		rdbtnNewRadioButton_3.setBounds(689, 297, 88, 18);
		contentPane.add(rdbtnNewRadioButton_3);
		rdbtnNewRadioButton_3.addActionListener(this);
		
		JLabel lblBamkiLogiczne = new JLabel("Bamki logiczne:");
		lblBamkiLogiczne.setBounds(640, 373, 121, 16);
		contentPane.add(lblBamkiLogiczne);
		
		// grupa bramek logicznych tylko jedna do wyboru
		bgBramki.add(rdbtnNewRadioButton_4);
		bgBramki.add(rdbtnNewRadioButton_5);
		bgBramki.add(rdbtnNewRadioButton_6);
		bgBramki.add(rdbtnNewRadioButton_7);
		
		JRadioButton rdbtnNewRadioButton_4 = new JRadioButton("Diode");
		rdbtnNewRadioButton_4.setBounds(612, 401, 115, 18);
		contentPane.add(rdbtnNewRadioButton_4);
		rdbtnNewRadioButton_4.addActionListener(this);
		
		JRadioButton rdbtnNewRadioButton_5 = new JRadioButton("OR");
		rdbtnNewRadioButton_5.setBounds(612, 431, 115, 18);
		contentPane.add(rdbtnNewRadioButton_5);
		rdbtnNewRadioButton_5.addActionListener(this);
		
		JRadioButton rdbtnNewRadioButton_6 = new JRadioButton("AND - NOT");
		rdbtnNewRadioButton_6.setBounds(612, 461, 115, 18);
		contentPane.add(rdbtnNewRadioButton_6);
		rdbtnNewRadioButton_6.addActionListener(this);
		
		JRadioButton rdbtnNewRadioButton_7 = new JRadioButton("FLIP - FLOP");
		rdbtnNewRadioButton_7.setBounds(612, 491, 115, 18);
		contentPane.add(rdbtnNewRadioButton_7);
		
		// interwal domyslnie 1 sekunda
		
		
		rdbtnSekunda = new JRadioButton("1 sekunda", true);
		rdbtnSekunda.setBounds(227, 612, 88, 18);
		contentPane.add(rdbtnSekunda);
		
		JRadioButton rdbtnPSekundy = new JRadioButton("p\u00F3\u0142 sekundy");
		rdbtnPSekundy.setBounds(327, 612, 100, 18);
		contentPane.add(rdbtnPSekundy);
		
		JRadioButton rdbtnSekundy = new JRadioButton("1/8 sekundy");
		rdbtnSekundy.setBounds(439, 612, 100, 18);
		contentPane.add(rdbtnSekundy);
		rdbtnNewRadioButton_7.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object wybrane = e.getSource();
	
		if(wybrane == mntmOtworzPlik ){
			
		}
		
	}
}
