package wireworld;

import java.awt.Color;

public interface Stan {

	public Stan nowyStan(int i);

	public Color zwrocKolor();

}
