package wireworld;

public class Wireworld {

	public Plansza plansza;
	public Plansza plansza2;

	public Wireworld(Plansza plansza) {
		this.plansza = plansza;
		plansza2 = new Plansza(60, 60);
	}

	public Plansza nastepnaGeneracja(Plansza plansza) {
		Sasiedztwo otoczenie = new Sasiedztwo();
		Stan stan = null;
		Stan stan2 = null;
		int g = 0;
		for (int i = 0; i < plansza.zwrocWiersze(); i++) {
			for (int j = 0; j < plansza.zworcKolumny(); j++) {
				g = otoczenie.sasiedztwo(i, j, plansza);
				stan = plansza.pobierzStan(i, j);
				stan2 = stan.nowyStan(g);
				plansza2.siatka[i][j].ustawStan(stan2);
			}
		}
		return plansza2;
	}

}