package wireworld;

public class Sasiedztwo {

	public int sasiedztwo(int x, int y, Plansza plansza) {
		int glowy = 0;
		int i, j = 0;
		for (i = x - 1; i <= x + 1; i++) {
			if (i < 0 || i >= plansza.zwrocWiersze()) {
				continue;
			}
			for (j = y - 1; j <= y + 1; j++) {
				if ((i == x && j == y) || j < 0 || j >= plansza.zworcKolumny()) {
					continue;
				}

				if (plansza.siatka[i][j].zwrocStan().equals("Glowa")) {
					glowy++;
				}
			}
		}
		return glowy;
	}

}
