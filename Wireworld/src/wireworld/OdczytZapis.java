package wireworld;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public class OdczytZapis {

	public Plansza wczytajPlik(File nazwaPliku) throws IOException {
		Plansza plansza = new Plansza(60, 60);
		BufferedReader odczyt = null;
		int i =0;
		int j = 0;
		try {
			odczyt = new BufferedReader(new FileReader(nazwaPliku));
			String bufor;
			while ((bufor = odczyt.readLine()) != null) {
				String[] p = bufor.split("\\s+");
				try {
					// String nazwaElementu = p[0];
					// Stan stan = FabrykaStanow.buduj(nazwaElementu);
					i = Integer.parseInt(p[1]);
					j = Integer.parseInt(p[2]);
					switch (p[0]) {
					case "Glowa":
						plansza.siatka[i][j].ustawStan(new Glowa());
					
						break;
					case "Przewodnik":
						plansza.siatka[i][j].ustawStan(new Przewodnik());
					
						break;
					case "Ogon":
						plansza.siatka[i][j].ustawStan(new Ogon());
			
						break;
					case "Pusta":
						plansza.siatka[i][j].ustawStan(new Pusta());
						
						break;
					default:
						System.err.println("Pominieto linie  \"" + bufor
								+ "\" nie istnieje taki element");
						continue;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("Pominieto linie \"" + bufor
							+ "\" zbyt malo pol");
				} catch (NumberFormatException e) {
					System.err.println("Pominieto linie \"" + bufor + "\" - \""
							+ p[2] + "\" nie jest liczba calkowita");
				} i++;
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} finally {
			if (odczyt != null)
				odczyt.close();
		}
		return plansza;
	}

	public void zapiszPlik(Plansza plansza, File nazwaPliku) throws IOException {
		try (PrintStream wyjscie = new PrintStream(nazwaPliku)) {
			for (int i = 0; i < plansza.zwrocWiersze(); i++) {
				for (int j = 0; j < plansza.zworcKolumny(); j++) {
					wyjscie.print(plansza.siatka[i][j]);
					wyjscie.println(" " + i + " " + j);
				}
			}
		}
	}
}
