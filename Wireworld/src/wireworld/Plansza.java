package wireworld;

public class Plansza {

	public Komorka siatka[][];
	private int wiersze;
	private int kolumny;

	public Plansza(int w, int c) {
		wiersze = w;
		kolumny = c;
		siatka = new Komorka[w][c];
		for (int i = 0; i < w; i++)
			for (int j = 0; j < c; j++) {
				siatka[i][j] = new Komorka(new Pusta());
			}
	}

	public int zwrocWiersze() {
		return wiersze;
	}

	public int zworcKolumny() {
		return kolumny;
	}

	public void wyczyscPlansze() {
		for (int i = 0; i < this.wiersze; i++)
			for (int j = 0; j < this.kolumny; j++) {
				this.siatka[i][j] = new Komorka(new Pusta());
			}
	}

	public Stan pobierzStan(int i, int j) {
		return this.siatka[i][j].stanKomorki;
	}
	

	
	public void wydrukuj(){
		for (int i = 0; i < this.zwrocWiersze(); i++) {
			for (int j = 0; j < this.zworcKolumny(); j++) {
				System.out.print(this.siatka[i][j]);
				System.out.print(" ");
			}
			System.out.println("");
		}
	}
	
	public Plansza kopjuj(Plansza plansza){
		Plansza plansza2 = new Plansza(60,60);
		for(int i =0 ; i < plansza.zwrocWiersze() ; i ++){
			for(int j = 0; j <  plansza.zworcKolumny(); j ++){
				plansza2.siatka[i][j].stanKomorki = plansza.pobierzStan(i, j);
			}
		}
		return plansza2;
	}

}
