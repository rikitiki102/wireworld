package wireworld;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {

		OdczytZapis wczytaj = new OdczytZapis();
		Plansza plansza = new Plansza(60, 60);
		Plansza plansza2 = new Plansza(60, 60);
		File plik = new File(args[0]);
		try {
			plansza = wczytaj.wczytajPlik(plik);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Wireworld gra = new Wireworld(plansza);
		
		for(int i =0; i < 4 ; i++){
			System.out.println("generacja nr " + ( i + 1) );
			plansza2 = gra.nastepnaGeneracja(plansza);
			plansza2.wydrukuj();
			plansza = plansza2.kopjuj(plansza2);
		}
		

	}

}
