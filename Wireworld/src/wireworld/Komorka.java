package wireworld;

public class Komorka {

	Stan stanKomorki;

	public Komorka(Stan stan) {
		stanKomorki = stan;
	}

	public void ustawStan(Stan stan) {
		stanKomorki = stan;
	}

	public String zwrocStan() {
		return stanKomorki.getClass().getSimpleName();
	}

	public String toString() {
		return this.zwrocStan();
	}

}
